package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_profile);
        TextView nameAnimal = findViewById(R.id.animalName);
        TextView descAnimal = findViewById(R.id.description);
        ImageView imageAnimal = findViewById(R.id.ImageView);

        String animalName = "Animal Name Not Found";
        String animalDescription = "Animal description not Found";
        String animalView = "Animal Resource Not Found";
        int position = 0;

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            animalName = extras.getString("Key");
            animalDescription = extras.getString("Desc");
            // animalView = extras.getString("View");
            position = extras.getInt("id");
        }

        if (position == 0){
            imageAnimal.setImageResource(R.drawable.kermit);
        } else if (position == 1){
            imageAnimal.setImageResource(R.drawable.remi);
        } else if (position == 2){
            imageAnimal.setImageResource(R.drawable.stuart);
        } else if (position == 3){
            imageAnimal.setImageResource(R.drawable.gojira);
        } else if (position == 4){

            imageAnimal.setImageResource(R.drawable.shaun);
        }

        nameAnimal.setText(animalName);
        descAnimal.setText(animalDescription);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.information:
                Intent info = new Intent(Profile.this, Information.class);
                startActivity(info);
                return true;
            case R.id.uninstall:
                Intent uninstall = new Intent(Intent.ACTION_DELETE, Uri.parse("package:edu.sjsu.android.zoodirectory"));
                startActivity(uninstall);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
