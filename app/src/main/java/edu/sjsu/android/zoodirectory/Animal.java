package edu.sjsu.android.zoodirectory;

public class Animal {
    private String animalName;
    private String animalDescription;
    private String animalView;

    public Animal(String animalName, String animalDescription, String animalView){
        this.animalName = animalName;
        this.animalDescription = animalDescription;
        this.animalView = animalView;
    }

    public String getAnimalView() {
        return animalView;
    }

    public void setAnimalView(String animalView) {
        this.animalView = animalView;
    }

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public String getAnimalDescription() {
        return animalDescription;
    }

    public void setAnimalDescription(String animalDescription) {
        this.animalDescription = animalDescription;
    }
}
