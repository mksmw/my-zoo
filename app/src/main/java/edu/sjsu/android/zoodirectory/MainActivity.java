package edu.sjsu.android.zoodirectory;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Animal> animalArrayList;
    private RecyclerAdapter.RecyclerViewClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerview);
        animalArrayList = new ArrayList<>();
        setAnimalName();
        setAdapter();
    }

    private void setAdapter() {
        setOnClickListener();
        RecyclerAdapter adapter = new RecyclerAdapter(animalArrayList, listener);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setOnClickListener() {
        listener = new RecyclerAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(getApplicationContext(), Profile.class);
                intent.putExtra("Key", animalArrayList.get(position).getAnimalName());
                intent.putExtra("Desc", animalArrayList.get(position).getAnimalDescription());
                intent.putExtra("View", animalArrayList.get(position).getAnimalView());
                intent.putExtra("id", position);

                if (position == 4){
                    dialogBox(intent);
                } else {
                    startActivity(intent);
                }
            }
        };
    }

    private void setAnimalName(){
        animalArrayList.add(new Animal("Kermit", "Kermit the Frog is a Muppet character created " +
                "and originally performed by Jim Henson. Introduced in 1955, Kermit serves as the straight man protagonist " +
                "of numerous Muppet productions, most notably Sesame Street and The Muppet Show, as well as in other television " +
                "series, feature films, specials, and public service announcements through the years.", "kermit"));
        animalArrayList.add(new Animal("Remi", "Remy is a rat who simply adores food and its quality. " +
                "He befriends a spectral representation of his hero, the late Auguste Gusteau. Due to his love for food, " +
                "he has a strong sense of smell, and his father Django appoints him as an inspector amongst the rat's clan " +
                "after Remy stops him from eating a poisoned apple core, so the rats will no longer die to toxic food.", "remi"));
        animalArrayList.add(new Animal("Stuart", "In the novel, Stuart was born to the Littles, " +
                "and the family adapted to having such a small son. He could walk as soon as he was born, and he could climb " +
                "lamps by shinnying the cord at one week old. The family was then concerned about mouse references in literature, " +
                "such as ripping out the Three Blind Mice page in their nursery rhymes book, and changing the word mouse to louse " +
                "in The Night Before Christmas.", "stuart"));
        animalArrayList.add(new Animal("Godzilla", "Godzilla is an enormous, destructive, prehistoric " +
                "sea monster awakened and empowered by nuclear radiation. With the nuclear bombings of Hiroshima and Nagasaki " +
                "and the Lucky Dragon 5 incident still fresh in the Japanese consciousness, Godzilla was conceived as a " +
                "metaphor for nuclear weapons.[29] Others have suggested that Godzilla is a metaphor for the United States, " +
                "a giant beast woken from its slumber which then takes terrible vengeance on Japan.", "gojira"));
        animalArrayList.add(new Animal("Shaun", "Shaun is the titular main character and protagonist " +
                "of Shaun the Sheep. He is the leader of The Flock. A mild-mannered yet extraordinary creature, Shaun exhibits" +
                " typical human behavior, which causes the other sheep to view him as their leader in times of confusion.","shaun"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.information:
                Intent info = new Intent(MainActivity.this, Information.class);
                startActivity(info);
                return true;
            case R.id.uninstall:
                Intent uninstall = new Intent(Intent.ACTION_DELETE, Uri.parse("package:edu.sjsu.android.zoodirectory"));
                startActivity(uninstall);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void dialogBox(final Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("This animal is said to kill 500 people a year, very scary animal! You sure want to continue?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(intent);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
            }
        }).setCancelable(false);
        builder.create().show();

    }
}