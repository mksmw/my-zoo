package edu.sjsu.android.zoodirectory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<Animal> animalArrayList;
    private RecyclerViewClickListener listener;

    public RecyclerAdapter(ArrayList<Animal> animalArrayList, RecyclerViewClickListener listener){
        this.animalArrayList = animalArrayList;
        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView animalName;
        private ImageView animalView;
        
        public MyViewHolder(final View view){
            super(view);
            animalName = view.findViewById(R.id.animalTitle);
            animalView = view.findViewById(R.id.animalImage);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.animals_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyViewHolder holder, int position) {
        String name = animalArrayList.get(position).getAnimalName();
        holder.animalName.setText(name);

        if (position == 0) {
            holder.animalView.setImageResource(R.drawable.kermit);
        } else if (position == 1){
            holder.animalView.setImageResource(R.drawable.remi);
        } else if (position == 2){
            holder.animalView.setImageResource(R.drawable.stuart);
        } else if (position == 3){
            holder.animalView.setImageResource(R.drawable.gojira);
        } else if (position == 4){
            holder.animalView.setImageResource(R.drawable.shaun);
        }
    }

    @Override
    public int getItemCount() {
        return animalArrayList.size();
    }

    public interface RecyclerViewClickListener{
        void onClick(View v, int position);
    }
}
